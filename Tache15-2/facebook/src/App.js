import React from "react";
import logoFb from "./img/facebook-logo.png";
import imgFbUser from "./img/facebook-users-map.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

function App() {
  
  const listeLiensLangues = [
    { hrefValue: "#", titleValue: "Fula", value: "Fula" },
    { hrefValue: "#", titleValue: "English (US)", value: "English (US)" },
    { hrefValue: "#", titleValue: "Italian", value: "Italiano" },
    {
      hrefValue: "#",
      titleValue: "Spanish (Spain)",
      value: "Español (España)",
    },
    { hrefValue: "#", titleValue: "Arabic", value: "العربية" },
    {
      hrefValue: "#",
      titleValue: "Portuguese (Portugal)",
      value: "Português (Portugal)",
    },
    { hrefValue: "#", titleValue: "German", value: "Deutsch" },
    { hrefValue: "#", titleValue: "Hindi", value: "हिन्दी" },
    {
      hrefValue: "#",
      titleValue: "Simplified Chinese (China)",
      value: "中文(简体)",
    },
    { hrefValue: "#", titleValue: "Japanese", value: "日本語" },
  ];

  function UlBlockFooter({}) {
    return (
      <ul>
        <li>Français (France)</li>
        {listeLiensLangues.map((lien) => (
          <li>
            <a href={lien.hrefValue} title={lien.titleValue}>
              {lien.value}
            </a>
          </li>
        ))}
        <li>
          <FontAwesomeIcon icon={["fas", "plus"]} size="" />
        </li>
      </ul>
    );
  }

  const listeLiensExternes = [
    { hrefValue: "#", titleValue: "Inscription", value: "Inscription" },
    { hrefValue: "#", titleValue: "Connexion", value: "Connexion" },
    { hrefValue: "#", titleValue: "Essayez Messenger.", value: "Messenger" },
    {
      hrefValue: "#",
      titleValue: "Facebook Lite pour Android.",
      value: "Facebook Lite",
    },
    {
      hrefValue: "#",
      titleValue: "Naviguez parmi nos vidéos Watch.",
      value: "Watch",
    },
    {
      hrefValue: "#",
      titleValue: "Parcourez notre annuaire de personnes.",
      value: "Personnes",
    },
    {
      hrefValue: "#",
      titleValue: "Parcourez notre annuaire des Pages.",
      value: "Pages",
    },
    {
      hrefValue: "#",
      titleValue: "Catégories de Page",
      value: "Catégories de Page",
    },
    {
      hrefValue: "#",
      titleValue: "Découvrez les lieux populaires sur Facebook.",
      value: "Lieux",
    },
    {
      hrefValue: "#",
      titleValue: "Découvrez les jeux Facebook.",
      value: "Jeux",
    },
    {
      hrefValue: "#",
      titleValue: "Parcourez notre liste de lieux.",
      value: "Lieux",
    },
    {
      hrefValue: "#",
      titleValue: "Achetez et vendez sur Facebook Marketplace.",
      value: "Marketplace",
    },
    {
      hrefValue: "#",
      titleValue: "En savoir plus sur Facebook Pay",
      value: "Facebook Pay",
    },
    {
      hrefValue: "#",
      titleValue: "Parcourez notre annuaire des groupes.",
      value: "Groupes",
    },
    {
      hrefValue: "#",
      titleValue: "En savoir plus sur Oculus",
      value: "Oculus",
    },
    {
      hrefValue: "#",
      titleValue: "En savoir plus sur Portal from Facebook",
      value: "Portal",
    },
    { hrefValue: "#", titleValue: "Découvrez Instagram", value: "Instagram" },
    {
      hrefValue: "#",
      titleValue: "Parcourir notre annuaire de listes locales.",
      value: "Local",
    },
    {
      hrefValue: "#",
      titleValue: "Faire un don à des causes qui le méritent.",
      value: "Collectes de fonds",
    },
    {
      hrefValue: "#",
      titleValue: "Parcourir notre annuaire des services Facebook.",
      value: "Services",
    },
    {
      hrefValue: "#",
      titleValue:
        "Consultez notre blog, découvrez notre centre de ressources et recherchez des offres d’emploi.Consultez notre blog, découvrez notre centre de ressources et recherchez des offres d’emploi.",
      value: "À propos",
    },
    {
      hrefValue: "#",
      titleValue: "Diffusez votre publicité sur Facebook.",
      value: "Créer une publicité",
    },
    { hrefValue: "#", titleValue: "Créez une Page.", value: "Créer une Page" },
    {
      hrefValue: "#",
      titleValue: "Développez sur notre propre plate-forme",
      value: "Développeurs",
    },
    {
      hrefValue: "#",
      titleValue:
        "Faites évoluer votre carrière en rejoignant notre incroyable entreprise.",
      value: "Emplois",
    },
    {
      hrefValue: "#",
      titleValue:
        "En savoir plus sur Facebook et le respect de votre vie privée.",
      value: "Confidentialité",
    },
    {
      hrefValue: "#",
      titleValue: "À propos des cookies et de Facebook.",
      value: "Cookies",
    },
    {
      hrefValue: "#",
      titleValue: "En savoir plus sur Choisir sa pub.",
      value: "Choisir sa pub",
    },
    {
      hrefValue: "#",
      titleValue:
        "Prenez connaissance des conditions générales et des règlements.",
      value: "Conditions générales",
    },
    {
      hrefValue: "#",
      titleValue: "Consultez les pages d’aide.",
      value: "Aide",
    },
    {
      hrefValue: "#",
      titleValue: "Affichez et modifiez vos paramètres Facebook.",
      value: "Paramètres",
    },
    {
      hrefValue: "#",
      titleValue: "Affichez votre historique personnel",
      value: "Historique personnel",
    },
  ];

  function UlBlockFooter2({}) {
    return (
      <ul>
        {listeLiensExternes.map((lien) => (
          <li>
            <a href={lien.hrefValue} title={lien.titleValue}>
              {lien.value}
            </a>
          </li>
        ))}
      </ul>
    );
  }

  return (
    <div className="App">
      <header>
        <div className="container">
          <nav className="navbar navbar-expand-lg">
            <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
              <a className="navbar-brand" href="#">
                <img src={logoFb}></img>
              </a>
              <form className="ml-auto">
                <div className="form-row">
                  <div className="form-group">
                    <label for="inputEmail4">Adresse e-mail ou mobile</label>
                    <input type="email" className="" id="inputEmail4"></input>
                  </div>
                  <div className="form-group">
                    <label for="inputPassword4">Mot de passe</label>
                    <input
                      type="password"
                      className=""
                      id="inputPassword4"
                    ></input>
                    <small
                      id="passwordHelpBlock"
                      style={{ display: "block" }}
                      className="text-muted"
                    >
                      <a href="#">Informations de compte oubliées ?</a>
                    </small>
                  </div>
                  <div className="form-group">
                    <label for="inputPassword4">&nbsp;</label>
                    <button className="btn-connect" type="submit">
                      Connexion
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </nav>
        </div>
      </header>

      <section>
        <div className="container">
          <div className="row">
            <div className="col-6" id="left-section">
              <h3 className="">
                Avec Facebook, partagez et restez en contact avec votre
                entourage.
              </h3>
              <img src={imgFbUser}></img>
            </div>
            <div className="col-5 offset-1" id="right-section">
              <h3>Inscription</h3>
              <p className="decor-1">C’est rapide et facile.</p>
              <form>
                <div className="form-group">
                  <div className="form-row">
                    <div className="col">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Prénom"
                      ></input>
                    </div>
                    <div className="col">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Nom de famille"
                      ></input>
                    </div>
                  </div>
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Numéro de mobile ou email"
                  ></input>
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Nouveau mot de passe"
                  ></input>
                </div>
                <div className="form-group">
                  <div className="form-label-1">Date de naissance</div>
                  <div className="form-inline">
                    <select className="" id="">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                    <select className="" id="">
                      <option>jan</option>
                      <option>fév</option>
                      <option>mar</option>
                      <option>avr</option>
                      <option>mai</option>
                      <option>jun</option>
                      <option>juil</option>
                      <option>aou</option>
                      <option>sep</option>
                      <option>oct</option>
                      <option>nov</option>
                      <option>déc</option>
                    </select>
                    <select className="" id="">
                      <option>2020</option>
                      <option>2019</option>
                      <option>2018</option>
                      <option>2017</option>
                      <option>2016</option>
                    </select>
                    <span style={{ color: "grey" }}>
                      &nbsp;&nbsp;
                      <FontAwesomeIcon
                        icon={["fas", "question-circle"]}
                        size=""
                      />
                    </span>
                  </div>
                </div>
                <div className="form-group">
                  <div className="form-label-1">Genre</div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions"
                      id="inlineRadio1"
                      value="option1"
                    ></input>
                    <label className="form-check-label" for="inlineRadio1">
                      Femme
                    </label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions"
                      id="inlineRadio2"
                      value="option2"
                    ></input>
                    <label className="form-check-label" for="inlineRadio2">
                      Homme
                    </label>
                  </div>
                  <div className="form-check form-check-inline">
                    <input
                      className="form-check-input"
                      type="radio"
                      name="inlineRadioOptions"
                      id="inlineRadio3"
                      value="option3"
                    ></input>
                    <label className="form-check-label" for="inlineRadio3">
                      Personnalisé
                    </label>
                  </div>
                  <span style={{ color: "grey" }}>
                    <FontAwesomeIcon
                      icon={["fas", "question-circle"]}
                      size=""
                    />
                  </span>
                </div>
                <div className="details col-8">
                  <p>
                    En appuyant sur Inscription, vous acceptez nos{" "}
                    <a href="#">Conditions générales</a>, notre{" "}
                    <a href="#">Politique d’utilisation des données</a> et notre{" "}
                    <a href="#">Politique d’utilisation des cookies</a>. Vous
                    recevrez peut-être des notifications par texto de notre part
                    et vous pouvez à tout moment vous désabonner.
                  </p>
                </div>
                <div className="form-group">
                  <button type="submit" className="btn-inscription">
                    Inscription
                  </button>
                </div>
                <div className="form-group">
                  <p id="creer-page-text">
                    <a href="#">Créer une Page</a> pour une célébrité, un groupe
                    ou une <br></br> entreprise.
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>

      <footer className="">
        <div className="container">
          <div className="row">
            <div id="langues">
              <UlBlockFooter></UlBlockFooter>
            </div>
            <div className="hr-footer-separator"></div>
            <div id="liens-fb">
              <UlBlockFooter2></UlBlockFooter2>
            </div>
            <div className="copyright">
              <div>
                <span> Facebook © 2020</span>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}

export default App;
