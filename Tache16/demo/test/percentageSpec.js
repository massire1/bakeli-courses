//var assert = require('assert');
require('chai').should()
var assert = require('chai').assert;
var expect = require('chai').expect;
var Percentage = require('../lib/Percentage')

describe('Percentage', function() {

    describe('#evolution', function() {

        it.skip('should give an evolution', function() {
            //assert.equal(Percentage.evolution(100, 200), 100)
            //assert.equal(Percentage.evolution(100, 150), 50)
            //assert.equal(Percentage.evolution(100, 50), -50)
            expect(Percentage.evolution(100, 200)).be.equal(100)
            expect(Percentage.evolution(100, 150)).be.equal(50)
            expect(Percentage.evolution(100, 50)).be.equal(-50)
        })

        it('should handle 0 evolution', function() {
            //assert.equal(Percentage.evolution(0, 100), Infinity)
            expect(Percentage.evolution(0, 100)).to.be.equal(Infinity)
        })

        it('should round values', function() {
            //assert.equal(Percentage.evolution(30, 100), 233.33)
            expect(Percentage.evolution(30, 100)).to.be.equal(233.33)
        })

    })

    describe('#wait', function() {

        it('should exit', function() {
            assert.isFunction(Percentage.wait)
        })

        it('should wait', function(done) {
            Percentage.wait(50, function(test) {
                assert.equal(test, 18);
                done();
            })
        })
    })
})