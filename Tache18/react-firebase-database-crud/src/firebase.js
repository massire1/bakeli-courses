//import * as firebase from "firebase";
import firebase from 'firebase/app'

import "firebase/database";

let config = {
  apiKey: "AIzaSyB0pWL16ntIfL5SzzR3s0Cx0JaCu3V7V1U",
  authDomain: "bezkoder-firebas.firebaseapp.com",
  databaseURL: "https://bezkoder-firebas-default-rtdb.firebaseio.com/",
  projectId: "bezkoder-firebas",
  storageBucket: "bezkoder-firebas.appspot.com",
  messagingSenderId: "521784377256",
  appId: "1:521784377256:web:47ba1ea10dcd491565279d",
};

firebase.initializeApp(config);

export default firebase.database();
