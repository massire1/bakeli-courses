import React, { useContext } from "react";
import { Router, Link } from "@reach/router";
import NavbarPage from "./NavbarPage";
import Menu from "./Menu";
import Cours from "./Cours";
import Professeurs from "./Professeurs";
import Dashboard from "./Dashboard";

const MainLayout = ({ navbarTop, menu, content, children, ...restProps }) => {
  const classes = "myClasses";
  return (
    <div>
      <NavbarPage />
      <div className="container-fluid" style={{ marginTop: 30 }}>
        {children}
      </div>
    </div>
  );
};

export default MainLayout;
