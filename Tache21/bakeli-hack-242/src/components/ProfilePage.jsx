import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import { navigate } from "@reach/router";
import { auth } from "../firebase";

const ProfilePage = () => {
  const user = useContext(UserContext);
  const { photoURL, displayName, email } = user.data;
  //console.log("user connected " + user);

  return (
    <div>
      <nav className="navbar navbar-default">
        <a href="/" className="navbar-brand">
          Bakeli-Hack-242
        </a>
        <div className="navbar-nav mr-auto"></div>
        <button type="button" className="btn btn-default navbar-btn"
          onClick={() => {
            auth.signOut();
          }}
        >
          {email}, Déconnexion
        </button>
      </nav>

      <div className="row" style={{ marginTop: 30 }}>
        <div className="col-3">
          <div className="list-group">
            <a href="#" className="list-group-item active">
              Gestion des Cours
            </a>
            <a href="#" className="list-group-item">
              Gestion des Professseurs
            </a>
            <a href="#" className="list-group-item">
              Gestion des Apprenants
            </a>
            <a href="#" className="list-group-item">
              Paramètres
            </a>
          </div>
        </div>

        <div className="col-9">
          <div className="jumbotron">
            <h1>Dashboard Home</h1>
            <p>...</p>
            <p>
              <a className="btn btn-primary btn-lg" href="#" role="button">
                Learn more
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfilePage;
