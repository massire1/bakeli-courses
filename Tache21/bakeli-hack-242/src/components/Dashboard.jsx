import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import { auth } from "../firebase";
import Menu from "./Menu";
import Navbar from "./NavbarPage";
import MainLayout from "./MainLayout";

const Dashboard = () => {
  //const user = useContext(UserContext);

  return (
    <div className="row">
      <div className="col-3">
        <Menu activeMenuNumber="0" />
      </div>
      <div className="col-9">
        <div className="jumbotron">
          <h1>Dashboard</h1>
          <p>...</p>
          <p>
            <a className="btn btn-primary btn-lg" href="#" role="button">
              Learn more
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
