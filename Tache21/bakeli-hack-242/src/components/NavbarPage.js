import React, { useContext } from "react";
import { auth } from "../firebase";
import { UserContext } from "../providers/UserProvider";
import {
  Navbar,
  Nav,
  Button,
  FormControl,
  NavDropdown,
  Form,
} from "react-bootstrap";

const NavbarPage = ({ title, breadcrumbs, tag: Tag }) => {
  const user = useContext(UserContext);
  const { photoURL, displayName, email } = user.userInfos;
  console.log("user connected " + user);

  return (
    <Navbar bg="light" expand="lg" sticky="top">
      <Navbar.Brand href="#">Bakeli-Hack-242</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          {/*<Nav.Link href="#home">Home</Nav.Link>*/}
          <Nav.Link href="#">{email}</Nav.Link>
          {/*<NavDropdown title={email} id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">
              Another action
            </NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="#action/3.4">
              Separated link
            </NavDropdown.Item>
          </NavDropdown>*/}
        </Nav>
        <Form inline>
          {/*<FormControl type="text" placeholder="Search" className="mr-sm-2" />*/}
          <Button
            variant="outline-success"
            onClick={() => {
              auth.signOut();
            }}
          >
            {displayName}, Déconnexion
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarPage;
