import React, { useContext, useState } from "react";
import { Link } from "@reach/router";
import { auth, signInWithGoogle, generateUserDocument } from "../firebase";

const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [error, setError] = useState(null);

  const createUserWithEmailAndPasswordHandler = async (
    event,
    email,
    password
  ) => {
    event.preventDefault();
    try {
      const { user } = await auth.createUserWithEmailAndPassword(email,password);
      generateUserDocument(user, { displayName });
    } catch (error) {
      setError("Error Signing up with email and password");
    }

    setEmail("");
    setPassword("");
    setDisplayName("");
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;

    if (name === "userEmail") {
      setEmail(value);
    } else if (name === "userPassword") {
      setPassword(value);
    } else if (name === "displayName") {
      setDisplayName(value);
    }
  };

  return (
    <div className="container">
      <div className="col-9 offset-2">
        <div className="col-sm-10">
          <h1 className="text-center">Inscription</h1>
        </div>
        <div className="">
          {error !== null && (
            <div className="form-group">
              <div className="col-sm-10">
                <div class="alert alert-danger .alert-dismissible" role="alert">
                  {error}
                </div>
              </div>
            </div>
          )}
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="displayName" class="col-sm-2 control-label">
                Pseudo
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  className="form-control"
                  name="displayName"
                  value={displayName}
                  placeholder=""
                  id="displayName"
                  onChange={(event) => onChangeHandler(event)}
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="userEmail" class="col-sm-2 control-label">
                Email
              </label>
              <div className="col-sm-10">
                <input
                  type="email"
                  className="form-control"
                  name="userEmail"
                  value={email}
                  placeholder=""
                  id="userEmail"
                  onChange={(event) => onChangeHandler(event)}
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="userPassword" class="col-sm-2 control-label">
                Mot de passe
              </label>
              <div className="col-sm-10">
                <input
                  type="password"
                  className="form-control"
                  name="userPassword"
                  value={password}
                  placeholder=""
                  id="userPassword"
                  onChange={(event) => onChangeHandler(event)}
                />
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button
                  className="btn btn-default btn-primary"
                  onClick={(event) => {
                    createUserWithEmailAndPasswordHandler(
                      event,
                      email,
                      password
                    );
                  }}
                >
                  Créer un compte
                </button>
              </div>
            </div>
          </form>
          {/*<p className="text-center my-3">or</p>
        <button
          onClick={() => {
            try {
              signInWithGoogle();
            } catch (error) {
              console.error("Error signing in with Google", error);
            }
          }}
          className="bg-red-500 hover:bg-red-600 w-full py-2 text-white"
        >
          Sign In with Google
        </button>*/}
          <p className="text-center">
            <Link to="/" className="my-2 text-blue-700 hover:text-blue-800 text-center block">
            &larr; back to sign in page
            </Link>{" "}
          </p>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
