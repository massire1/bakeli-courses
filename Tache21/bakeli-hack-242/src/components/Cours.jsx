import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import Menu from "./Menu";
import Table from "react-bootstrap/Table";
import Button from 'react-bootstrap/Button'

const Cours = () => {
  const user = useContext(UserContext);
  const coursDeLutilisateurs = user.userCours;
  //console.log("from cours ", coursDeLutilisateurs)

  return (
    <div className="row">
      <div className="col-3">
        <Menu activeMenuNumber="1" />
      </div>
      <div className="col-9">
        <ol className="breadcrumb">
          <li className="">
            <a href="#">Dashboard&nbsp;/&nbsp;</a>
          </li>
          <li className="active">Cours</li>
        </ol>
        <div className="">
          <p>
            <span className="float-left"><h1>Cours</h1></span>
            <span className="float-right"><Button variant="primary" size="sm">Ajouter Nouveau</Button></span>
          </p>
          <div>
            <Table responsive striped bordered hover size="sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Code</th>
                  <th>Libellé</th>
                  <th>Crédit</th>
                  <th>Module</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {coursDeLutilisateurs.map((cours, i) => (
                  <tr key={i}>
                    <td></td>
                    <td>{cours.code}</td>
                    <td>{cours.libelle}</td>
                    <td>{cours.credit}</td>
                    <td>{cours.module}</td>
                    <td>@</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cours;
