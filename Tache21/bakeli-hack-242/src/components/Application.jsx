import React, { Suspense, useContext } from "react";
import { Router } from "@reach/router";
import { UserContext } from "../providers/UserProvider";
import "bootstrap/dist/css/bootstrap.min.css";
import MainLayout from "./MainLayout";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import PasswordReset from "./PasswordReset";
import Parametres from "./Parametres";

const Dashboard = React.lazy(() => import("./Dashboard"));
const Cours = React.lazy(() => import("./Cours"));
const Professeurs = React.lazy(() => import("./Professeurs"));

function Application() {
  const user = useContext(UserContext);
  return user ? (
    <MainLayout>
      <Suspense fallback={<h1>Chargement de la page...</h1>}>
        <Router>
          <Dashboard exact path="/" />
          <Cours path="/cours" />
          <Professeurs path="/professeurs" />
          <Parametres path="/parametres" />
        </Router>
      </Suspense>
    </MainLayout>
  ) : (
    <Router>
      <SignUp path="signUp" />
      <SignIn path="/" />
      <PasswordReset path="passwordReset" />
    </Router>
  );
}

export default Application;
