import React, { useState } from "react";
import { Link } from "@reach/router";
import { signInWithGoogle } from "../firebase";
import { auth } from "../firebase";

const SignIn = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const signInWithEmailAndPasswordHandler = (event, email, password) => {
    event.preventDefault();
    auth.signInWithEmailAndPassword(email, password).catch((error) => {
      setError("Vos paramètres de connexion sont incorrects!");
      console.error("Paramètres de connexion incorrects", error);
    });
  };

  const onChangeHandler = (event) => {
    const { name, value } = event.currentTarget;

    if (name === "userEmail") {
      setEmail(value);
    } else if (name === "userPassword") {
      setPassword(value);
    }
  };
  console.log("React verion " + React.version);

  return (
    <div className="container">
      <div className="col-9 offset-2">
      <div className="col-sm-10"><h1 className="text-center">Se Connecter</h1></div>
        <div className="">
          {error !== null && (
            <div className="form-group">
              <div className="col-sm-10">
                <div className="alert alert-danger .alert-dismissible fade show" role="alert">{error}
                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
              </div>
            </div>
            
          )}
          <form className="form-horizontal">
            <div className="form-group">
              <label htmlFor="userEmail" class="col-sm-2 control-label">
                Email
              </label>
              <div className="col-sm-10">
                <input
                  type="email"
                  className="form-control"
                  id="userEmail"
                  name="userEmail"
                  placeholder=""
                  value={email}
                  onChange={(event) => onChangeHandler(event)}
                />
              </div>
            </div>

            <div className="form-group">
              <label htmlFor="userPassword" class="col-sm-2 control-label">
                Password
              </label>
              <div className="col-sm-10">
                <input
                  type="password"
                  className="form-control"
                  name="userPassword"
                  id="userEmail"
                  placeholder=""
                  value={password}
                  onChange={(event) => onChangeHandler(event)}
                />
              </div>
            </div>
            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <div className="checkbox">
                  <label>
                    <input type="checkbox" /> Remember me
                  </label>
                </div>
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-offset-2 col-sm-10">
                <button type="submit" className="btn btn-default btn-primary" onClick={(event) => {
                signInWithEmailAndPasswordHandler(event, email, password);
              }}>
                  Se Connecter
                </button>
              </div>
            </div>
          </form>
          {/*<p className="text-center my-3">or</p>
        <button
          className="bg-red-500 hover:bg-red-600 w-full py-2 text-white"
          onClick={() => {
            signInWithGoogle();
          }}
        >
          Sign in with Google
        </button>*/}
          <p className="text-center my-3">
            Vous n'êtes pas encore inscrits ?{" "}
            <Link to="signUp" className="text-blue-500 hover:text-blue-600">
              Nouveau compte içi
            </Link>{" "}
            <br />{" "}
            <Link
              to="passwordReset"
              className="text-blue-500 hover:text-blue-600"
            >
              Mot de passe oublié ?
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
