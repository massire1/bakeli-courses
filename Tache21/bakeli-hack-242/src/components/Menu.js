import React from "react";
const Menu = ({
  title,
  breadcrumbs,
  tag: Tag,
  className,
  children,
  activeMenuNumber,
  ...restProps
}) => {
  const classes = "myClasses";

  return (
    <div className="list-group">
      {activeMenuNumber && activeMenuNumber == "0" ? (
        <a href="/" className="list-group-item active">
          Dashboard
        </a>
      ) : (
        <a href="/" className="list-group-item">
          Dashboard
        </a>
      )}

      {activeMenuNumber && activeMenuNumber == "1" ? (
        <a href="/cours" className="list-group-item active">
          Gestion des Cours
        </a>
      ) : (
        <a href="/cours" className="list-group-item">
          Gestion des Cours
        </a>
      )}

      {activeMenuNumber && activeMenuNumber == "2" ? (
        <a href="/professeurs" className="list-group-item active">
          Professseurs
        </a>
      ) : (
        <a href="/professeurs" className="list-group-item">
          Professseurs
        </a>
      )}

      {activeMenuNumber && activeMenuNumber == "3" ? (
        <a href="/parametres" className="list-group-item active">
          Gestion des Apprenants
        </a>
      ) : (
        <a href="/parametres" className="list-group-item">
          Gestion des Apprenants
        </a>
      )}

      {activeMenuNumber && activeMenuNumber == "4" ? (
        <a href="/parametres" className="list-group-item active">
          Paramètres
        </a>
      ) : (
        <a href="/parametres" className="list-group-item">
          Paramètres
        </a>
      )}
    </div>
  );
};

export default Menu;
