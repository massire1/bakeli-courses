import React, { useContext } from "react";
import { UserContext } from "../providers/UserProvider";
import Menu from "./Menu";
import Navbar from "./NavbarPage";
import MainLayout from "./MainLayout";

const Professeurs = () => {
  //const user = useContext(UserContext);

  return (
    <div className="row">
      <div className="col-3">
        <Menu activeMenuNumber="2" />
      </div>
      <div className="col-9">
        <div className="jumbotron">
          <h1>Profs</h1>
          <p>...</p>
          <p>
            <a className="btn btn-danger btn-lg" href="#" role="button">
              Learn more
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Professeurs;
