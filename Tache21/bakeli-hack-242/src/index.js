import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import LogRocket from 'logrocket';
import "bootstrap/dist/css/bootstrap.min.css";

import App from './App';

ReactDOM.render(
  <BrowserRouter>
    <App />,
  </BrowserRouter>,
  document.getElementById('root')
);

//LogRocket.init('6wfj60/bakeli-hack-242');
//LogRocket.identify('mastaflex', {
//  name: 'Masta FLex',
//  email: 'diamasta@gmail.com',  
//  subscriptionType: 'pro'
//});