import logo from "./logo.svg";
import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import TutorialsList from "./components/tutorials-list.component";

function App() {
  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-dark">
        <a href="/tutorials" className="navbar-brand">
          bezKoder
        </a>
        <div className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link to={"/tutorials"} className="nav-link">
              Tutorials
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/tutorials"} className="nav-link">
              Add
            </Link>
          </li>
        </div>
      </nav>

      <div className="row" style={{ marginTop: 30 }}>
        <div className="col-3">
          <div class="list-group">
            <a href="#" class="list-group-item active">
              Dashboard Menu
            </a>
            <a href="#" class="list-group-item">
              Gestion des Cours
            </a>
            <a href="#" class="list-group-item">
              Gestion des Professseurs
            </a>
            <a href="#" class="list-group-item">
              Gestion des Apprenants
            </a>
            <a href="#" class="list-group-item">
              Paramètres
            </a>
          </div>
        </div>

        <div className="col-9">
          <div class="jumbotron">
            <h1>Dashboard Home</h1>
            <p>...</p>
            <p>
              <a class="btn btn-primary btn-lg" href="#" role="button">
                Learn more
              </a>
            </p>
          </div>
          <h2>React Firebase Database CRUD</h2>
          <Switch>
            <Route exact path={["/", "/tutorials"]} component={TutorialsList} />
            <Route exact path="/tutorials" component={TutorialsList} />
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default App;
