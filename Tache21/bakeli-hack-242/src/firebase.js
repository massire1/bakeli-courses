
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBXNJAawwrPjte90m4PRbzu8zys9V3DFF4",
  authDomain: "bakeli-hack-242.firebaseapp.com",
  databaseURL: "https://bakeli-hack-242-default-rtdb.firebaseio.com",
  projectId: "bakeli-hack-242",
  storageBucket: "bakeli-hack-242.appspot.com",
  messagingSenderId: "210940996093",
  appId: "1:210940996093:web:24d770a811b57f7ea7d194"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();

export const signInWithGoogle = () => {
  auth.signInWithPopup(provider);
};

export const generateUserDocument = async (user, additionalData) => {
  
  if (!user) return;

  const userRef = firestore.doc(`users/${user.uid}`);
  const snapshot = await userRef.get();

  if (!snapshot.exists) { // cas inscriptionn nouveau user
    const { email, displayName, photoURL } = user;
    try {
      await userRef.set({
        displayName,
        email,
        photoURL,
      });
    } catch (error) {
      console.error("Error creating user document", error);
    }
  }
  return getUserDocument(user.uid);
};

const getUserDocument = async (uid) => {

  if (!uid) return null;
  try {
    // recuperations infos user
    const document = await firestore.doc(`users/${uid}`).get();
    const userInfos = document.data();
    console.log("Database user:", userInfos);

    //recuperation cours du user 
    const document2 = await firestore.doc(`users/${uid}`).collection('cours').get();
    const userCours = []
    document2.forEach(collection => {
      console.log('Database user cours:', collection.data());
      userCours.push(collection.data());
    });
    
    return {
      userInfos,
      userCours
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};
