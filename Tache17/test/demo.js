var assert = require("assert");

describe("premier test", function () {
  it("should do something", function () {
    var x = 4;
    // verifier si l’assertion x + 2 = 6 est vraie;
    assert.strictEqual(x + 2, 6, "Le résultat est vrai");
    assert.strictEqual(x + 3, 6, "Le résultat est faux");
  });
});