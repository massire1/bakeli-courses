import React from "react";
import { icon, library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import johnDoeImg from "./john-doe2.png";

library.add(fab, fas, far);

function App() {
  function CardSection({ icon, cardTitle, cardText }) {
    return (
      <div className="mycard row">
        <div className="col-1">
          <div className="card-img text-center">{icon}</div>
        </div>
        <div className="col-11">
          <div className="card-text">
            <h3 className="card-title">{cardTitle}</h3>
            <p>{cardText}</p>
          </div>
        </div>
      </div>
    );
  }

  function WorkSection({ company, date, content }) {
    return (
      <div className="exp-content">
        <h5>{company}</h5>
        <p className="exp-date">{date}</p>
        <p>{content}</p>
        <hr></hr>
      </div>
    );
  }

  function UserInfosSection({ icon, content, mutedText }) {
    return (
      <div className="row user-infos">
        <div className="col-3 icon">
          <p className="text-center">{icon}</p>
        </div>
        <div className="col-9 text">
          <p>
            {content}
            <br></br>
            {mutedText}
          </p>
        </div>
      </div>
    );
  }

  return (
    <div className="container-fluid">
      <div className="row">

        {/*---------------- left sidebar section ----------------*/}
        <div className="col-4 sidebar-area">
          <div className="col-9 offset-3 photo">
            <img src={johnDoeImg} alt="" className="img-fluid"></img>
            <h1 className="name">Good Name</h1>
            <p>Designation</p>
          </div>

          <UserInfosSection
            icon={<FontAwesomeIcon icon={["fas", "phone"]} size="3x" />}
            content="221762631770"
            mutedText={<span className="muted">Mobile</span>}
          ></UserInfosSection>

          <UserInfosSection
            icon={<FontAwesomeIcon icon={["fas", "envelope"]} size="3x" />}
            content="name@gmail.com"
            mutedText={<span className="muted">Personal</span>}
          ></UserInfosSection>

          <UserInfosSection
            icon={<FontAwesomeIcon icon={["fas", "map-marker"]} size="3x" />}
            content="H-123, Block A"
            mutedText={<span className="muted">Mobile</span>}
          ></UserInfosSection>

          <UserInfosSection
            icon={<FontAwesomeIcon icon={["far", "calendar"]} size="3x" />}
            content="Technical Skills"
            mutedText={
              <div>
                <span className="muted">HTML5, CSS3, JQuery</span>
                <div className="col-10 progess-div">
                  <div className="progress">
                    <div
                      className="progress-bar progress-bar-striped"
                      role="progressbar"
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="50"
                    ></div>
                  </div>
                </div>
              </div>
            }
          ></UserInfosSection>

        </div>
        {/*---------------- end left sidebar section----------------*/}

        {/*---------------- right section section----------------*/}
        <div className="site-main col-8">
          <CardSection
            icon={<FontAwesomeIcon icon={["fas", "user"]} size="3x" />}
            cardTitle="About"
            cardText="Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus
            enim provident quas accusamus ut quasi similique ab quos tempore
            quisquam illum modi doloribus odio asperiores iure mollitia
            voluptatem, vitae ipsum molestias, aperiam, assumenda doloremque
            laboriosam fugit, sint rem quidem natus ab beatae, assumenda
            doloremque laboriosam fugit, sint rem quidem natus ab beatae.
            Quibusdam, rem!"
          ></CardSection>

          <CardSection
            icon={<FontAwesomeIcon icon={["fas", "suitcase"]} size="3x" />}
            cardTitle="Work Experience"
            cardText={
              <div>
                <WorkSection
                  company="Designation at Company Name"
                  date="APR 2015 - NOV 2017"
                  content="Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
              reprehenderit, eum impedit iure ullam minima? Mollitia tempore
              dignissimos ad vitae explicabo, Mollitia tempore dignissimos ad
              vitae explicabo"
                ></WorkSection>
                <WorkSection
                  company="Designation at Company Name"
                  date="APR 2015 - NOV 2017"
                  content="Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
              reprehenderit, eum impedit iure ullam minima? Mollitia tempore
              dignissimos ad vitae explicabo, Mollitia tempore dignissimos ad
              vitae explicabo"
                ></WorkSection>

              </div>
            }
          ></CardSection>

          <CardSection
            icon={
              <FontAwesomeIcon icon={["fas", "graduation-cap"]} size="3x" />
            }
            cardTitle="Education"
            cardText=""
          ></CardSection>
        </div>
        {/*---------------- end right section ----------------*/}
      </div>
    </div>
  );
}

export default App;
