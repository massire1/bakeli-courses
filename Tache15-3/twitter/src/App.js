import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";

library.add(fas, fab);

function App() {
  return (
    <div>
      <div className="container-fluid min-vh-100 d-flex flex-column">
        <div className="row flex-grow-1">
          <div id="left-bloc" className="col-6" style={{ overflow: "hidden"}}>
            <div id="svg-div">
              <svg viewBox="0 0 24 24" id="svg-img">
                <g>
                  <path d="M23.643 4.937c-.835.37-1.732.62-2.675.733.962-.576 1.7-1.49 2.048-2.578-.9.534-1.897.922-2.958 1.13-.85-.904-2.06-1.47-3.4-1.47-2.572 0-4.658 2.086-4.658 4.66 0 .364.042.718.12 1.06-3.873-.195-7.304-2.05-9.602-4.868-.4.69-.63 1.49-.63 2.342 0 1.616.823 3.043 2.072 3.878-.764-.025-1.482-.234-2.11-.583v.06c0 2.257 1.605 4.14 3.737 4.568-.392.106-.803.162-1.227.162-.3 0-.593-.028-.877-.082.593 1.85 2.313 3.198 4.352 3.234-1.595 1.25-3.604 1.995-5.786 1.995-.376 0-.747-.022-1.112-.065 2.062 1.323 4.51 2.093 7.14 2.093 8.57 0 13.255-7.098 13.255-13.254 0-.2-.005-.402-.014-.602.91-.658 1.7-1.477 2.323-2.41z"></path>
                </g>
              </svg>
            </div>
            <div className="row" style={{ padding: "100px" }}>
              <div className="col-8 offset-2" id="interets">
                <div className="row">
                  <p>
                    <FontAwesomeIcon icon={["fas", "search"]} size="1x" />
                    &nbsp;&nbsp;&nbsp;Suivez vos centres d'intérêt.
                  </p>
                </div>
                <div className="row">
                  <p>
                    <FontAwesomeIcon icon={["fas", "users"]} size="1x" />
                    &nbsp;&nbsp;&nbsp;Découvrez ce dont les gens parlent.
                  </p>
                </div>
                <div className="row">
                  <p>
                    <FontAwesomeIcon icon={["fas", "comment"]} size="1x" />
                    &nbsp;&nbsp;&nbsp;Rejoignez la conversation.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div id="right-bloc" className="col-6">
            <div className="row">
              <div className="col">
                <form>
                  <div className="row">
                    <div className="col-3 offset-3">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Téléphone, email, ou nom..."
                      ></input>
                    </div>
                    <div className="col-3">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Mot de passe"
                      ></input>
                      <small
                        id="passwordHelpBlock"
                        className="form-text text-muted"
                      >
                        <a href="#">Mot de passe oublié ?</a>
                      </small>
                    </div>
                    <div className="col-3">
                      <button type="button" className="btn join-btn2">
                        Se connecter
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="row" style={{ padding: "100px" }}>
              <div className="col-8 offset-2">
                <div className="">
                  <p className="primary-color1">
                    <FontAwesomeIcon icon={["fab", "twitter"]} size="3x" />
                  </p>
                  <p>
                    <h2 className="head-title">
                      Voir ce qui se passe actuellement dans le monde
                    </h2>
                  </p>
                </div>
                <div className="">
                  <p className="desc-text">
                    Rejoignez Twitter dès aujourd'hui.
                  </p>
                  <p>
                    <button
                      type="button"
                      href="#"
                      className="btn btn-lg btn-block join-btn1"
                      data-toggle="modal"
                      data-target="#exampleModalCenter"
                    >
                      S'inscrire
                    </button>
                    <a
                      type="button"
                      href="login.html"
                      className="btn btn-lg btn-block join-btn2"
                    >
                      Se connecter
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     {/* modal  */}
     <div className="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div className="modal-content">
                <div className="modal-body">
                    <div className="row" style={{margin: "0px", padding: "0px"}} >
                        <div className="col-4 offset-4">
                            <p className="text-center"><i className="fab fa-twitter fa-2x primary-color1"></i></p>
                        </div>
                        <div className="col-4" style={{padding: "0px 0px 0px 0px"}}>
                            <p className="text-right"><button type="button" className="btn btn-lg btn-sm join-btn1">Suivant</button></p>
                        </div>
                    </div>
                    <h4>Créer votre compte</h4>
                    <form id="login-form">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Nom et prénom"></input>
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Téléphone"></input>
                        </div>
                    </form>
                    <p style={{padding: "10px 10px"}}><a href="#" className="primary-color1">Utiliser un email</a></p>
                    <div>
                        <span style={{"font-weight": "bold"}}>Date de naissance</span><br></br>
                        <p className="text-muted">Cette information ne sera pas affichée publiquement. Confirmez votre âge, même si ce compte est pour une entreprise, un animal de compagnie ou autre chose.</p>
                    </div>
                    <form id="dateNaiss">
                        <div className="row">
                            <div className="col-5" style={{ padding: "0px 10px 0px 10px" }}>
                                <select className="" id="">
                                    <option selected value="">Mois</option>
                                    <option>Janvier</option>
                                    <option>Février</option>
                                    <option>Mars</option>
                                    <option>Avril</option>
                                    <option>Mai</option>
                                    <option>Juin</option>
                                    <option>Juillet</option>
                                    <option>Aout</option>
                                    <option>Septembre</option>
                                    <option>Octobre</option>
                                    <option>Nomvembre</option>
                                    <option>Décembre</option>
                                  </select>
                            </div>
                            <div className="col-3" style={{ padding: "0px 10px 0px 10px" }}>
                                <select className="" id="">
                                    <option selected value="">Jour</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>8</option>
                                    <option>10</option>
                                    <option>11</option>
                                    <option>12</option>
                                    <option>13</option>
                                    <option>14</option>
                                    <option>15</option>
                                    <option>16</option>
                                    <option>17</option>
                                    <option>18</option>
                                    <option>19</option>
                                    <option>20</option>
                                    <option>21</option>
                                    <option>22</option>
                                    <option>23</option>
                                    <option>24</option>
                                    <option>25</option>
                                    <option>26</option>
                                    <option>27</option>
                                    <option>28</option>
                                    <option>29</option>
                                    <option>30</option>
                                    <option>31</option>
                                  </select>
                            </div>
                            <div className="col-4" style={{ padding: "0px 10px 0px 10px" }}>
                                <select className="" id="">
                                    <option selected value="">Année</option>
                                    <option>2018</option>
                                    <option>2017</option>
                                    <option>2015</option>
                                    <option>2014</option>
                                    <option>2013</option>
                                    <option>2012</option>
                                    <option>2011</option>
                                    <option>2010</option>
                                    <option>2009</option>
                                    <option>2008</option>
                                    <option>2007</option>
                                  </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
     <footer className="fixed-bottom">
        <div className="container-fluid">
            <ul>
                <li><a href="#"> A propos</a></li>
                <li>
                    <a href="#">Centre d'assistance</a>
                </li>
                <li>
                    <a href="#">Conditions</a>
                </li>
                <li>
                    <a href="#">Politique de confidentialité</a>
                </li>
                <li>
                    <a href="#">Cookies</a>
                </li>
                <li>
                    <a href="#">Informations sur les publicités</a>
                </li>
                <li>
                    <a href="#">Blog</a>
                </li>
                <li>
                    <a href="#">Statut</a>
                </li>
                <li>
                    <a href="#">Tâches</a>
                </li>
                <li>
                    <a href="#">Marque</a>
                </li>
                <li>
                    <a href="#">Promouvoir</a>
                </li>
                <li>
                    <a href="#">Marketing</a>
                </li>
                <li>
                    <a href="#">Entreprises</a>
                </li>
                <li>
                    <a href="#">Développeurs</a>
                </li>
                <li>
                    <a href="#">Répertoires</a>
                </li>
                <li>
                    <a href="#">Paramètres</a>
                </li>
                <li>
                    <a href="#">&copy; 2020 Twitter, Inc.</a>
                </li>
            </ul>
        </div>
    </footer>
    </div>
  );
}

export default App;
